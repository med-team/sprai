sprai (0.9.9.23+dfsg1-3) unstable; urgency=medium

  * d/clean: new: fix double source build failure. (Closes: #1047125)
  * d/rules: activate hardwning flags.
  * flags.patch: propagate hardening flags to package targets.
    The patch is renamed from cflags.patch for naming consistency.
  * d/s/lintian-overrides: hide more unactionable hints.
  * d/control: bump to debhelper-compat 13.
  * d/control: declare compliance to standards version 4.7.0.
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Wed, 28 Aug 2024 21:31:44 +0200

sprai (0.9.9.23+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * Fix FTCBFS: use the makefile buildsystem. (Closes: #956946)

 -- Helmut Grohne <helmut@subdivi.de>  Fri, 17 Apr 2020 07:11:31 +0200

sprai (0.9.9.23+dfsg1-1) unstable; urgency=medium

  [ Adrian Bunk ]
  * Use CURDIR instead of PWD
    Closes: #956814

  [ Andreas Tille ]
  * Point Homepage to wayback machine
  * Deactivate watch file since upstream website vanished
  * Exclude waf from upstream source since it is unused
  * Remove debian/repack-waf since unused
  * Update debian/README.source

  [ Chris Lamb ]
  * Make the build reproducible
    Closes: #956473

 -- Andreas Tille <tille@debian.org>  Wed, 15 Apr 2020 17:01:52 +0200

sprai (0.9.9.23+dfsg-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Afif removed himself from Uploaders
  * Added myself to Uploaders
  * debhelper-compat 12
  * Standards-Version: 4.5.0
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.

  [ Antoni Villalonga ]
  * Migrate from WAF to plain Makefile
    Closes: #943280

  [ Andreas Tille ]
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 10 Apr 2020 10:03:27 +0200

sprai (0.9.9.23+dfsg-2) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Secure URI in copyright format
  * Remove trailing whitespace in debian/copyright
  * Fix Perl interpreter path

 -- Andreas Tille <tille@debian.org>  Mon, 07 Jan 2019 13:20:54 +0100

sprai (0.9.9.23+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Bump Standards-Version to 4.1.3

 -- Afif Elghraoui <afif@debian.org>  Sun, 04 Feb 2018 05:10:53 -0500

sprai (0.9.9.22+dfsg-1) unstable; urgency=low

  * Add example data
  * Bump copyright year
  * New upstream release
  * Update upstream contact email address

 -- Afif Elghraoui <afif@debian.org>  Fri, 20 Jan 2017 00:44:23 -0800

sprai (0.9.9.21+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.21+dfsg

 -- Afif Elghraoui <afif@debian.org>  Mon, 07 Nov 2016 21:02:54 -0800

sprai (0.9.9.19+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.19+dfsg
  * Refresh patch

 -- Afif Elghraoui <afif@debian.org>  Sat, 13 Aug 2016 23:41:00 -0700

sprai (0.9.9.18+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.18+dfsg

 -- Afif Elghraoui <afif@debian.org>  Wed, 22 Jun 2016 05:50:23 -0700

sprai (0.9.9.17+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.17+dfsg
  * Suggest installation of make

 -- Afif Elghraoui <afif@debian.org>  Thu, 09 Jun 2016 06:08:09 -0700

sprai (0.9.9.16+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.16+dfsg
  * Bump Standards-Version to 3.9.8
  * Refresh and update patch
  * Add documentation for new ezez4makefile_v4 command

 -- Afif Elghraoui <afif@debian.org>  Wed, 01 Jun 2016 23:13:26 -0700

sprai (0.9.9.14+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 0.9.9.14+dfsg
  * Refresh and update patch
  * Add README.source

 -- Afif Elghraoui <afif@debian.org>  Tue, 26 Apr 2016 19:42:24 -0700

sprai (0.9.9.13+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #820851)

 -- Afif Elghraoui <afif@debian.org>  Fri, 15 Apr 2016 01:21:05 -0700
